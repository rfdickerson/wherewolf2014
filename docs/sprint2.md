#Sprint 2 - Service layer#

- Due Oct 14. 5:00 PM
- You can work in groups (must report to me your group members)

###Description### 

In this sprint, you will create a web service API so that our
distributed clients can join and play in games. You can include your
own DAO or use the one I provide in Bitbucket.

###Prerequisites and setup###

* Download and install psycopg2

psycopg2 is a module for Python that allows your Python programs to
make calls to you PostgreSQL database.

* Download and install PostgreSQL

PostgreSQL is a relational database and is one of the leading open
source databases out there. We will use it because it contains some
useful geographic search modules that are useful for looking up the
location of a player. Postgresql should install some useful programs
(psql, createdb, initdb, createuser), that I will assume are in your
PATH. During installation, you will set your admin password for this
database, the default admin username is ‘postgres’. You can use the
createuser command to create a postgres user and a password should
your installation not provide one.

Once PostgreSQL is installed, create a new db called wherewolf by
using the 'createdb' function:

createdb wherewolf

* Load the schema that I wrote into your database using:

psql -d wherewolf -U postgres -f schema.sql

###Create the Web service API###

Included for you is a test script called client.py. When you run this
script, it will use the HTTP protocol to issue queries similar to what
your Android clients will eventually do.

The client script will register 8 new users in the game (michael,
dwight, jim, pam, ryan, andy, angela, toby). A new game called
NightHunt will be created by michael. michael will automatically be
added to that game. Next, all the other users will join that game,
creating new players for each. michael will set the game to active,
and the first day round begins. 30% of the players rounding up will be
set to be werewolves- 3 werewolves in our case.

There is no vote our first day round. Players will be randomly
positioned in a rectangular region. The admin sets the round to
night. One werewolf will move to a location of one random
villager. The werewolf will make an attack. The villager may or may
not survive this encounter, read ‘attack rules’. Admin sets the the
round to day. Every player will vote for a werewolf. Admin sets the
round to night. The werewolf will be locked up, leaving one
werewolf. Every player will set their positions to new random
positions in the zone. A werewolf will position itself near another
villager, and make an attack. Again, the villager might survive.

Hack- in order for our script to quickly test both the day and the
night cycle, and any temporal aspects of the game, we will need to add
to the API a command to change the game time to anything you set. This
can be complicated to reason about. What we will do, is for any code
you write that requires checking the time, such as attacks or voting,
we check the database for the current time field in the game table. If
it is set to NULL, then assume we just use the current system time for
calculating the time. Otherwise, use the value in the table for the
current time. When it comes time to deploy for production, we will
comment out this method.


* POST /register
will create a new user
payload (username, password, firstname, lastname)
returns:
 {'status': 'success'}
 failure conditions:
     user already exists with that username
     password too short (less than 6 characters)

* POST /game
will create a new game, sets user to admin
returns:
 {'status': 'success',
     'results': {'game_id': 202}}
 failure conditions:
     bad auth
     already administering a game

* DELETE /game/<gameid>
will delete the game only if user is the admin
returns:
 {'status': 'success'}
 failure conditions:
     bad auth
     not admin of the game

* POST /game/{gameid}/lobby
  joins a current game
  returns:
  {'status': 'success'}
  failure conditions:
      bad auth
      already in another game
      game not in the lobby mode (started or ended)

* PUT /game/{gameid}
  reports to the server your current position
  payload (lat, lng)
  returns:
  {'status': 'success',
      'results':
        {'werewolfscent':
            [{'player_id': 30, 'distance': 20},
            {'playerid': 31, 'distance': 33}]}}

* GET /game/{gameid}
  reports the current information about the game

* POST /game/{gameid}/ballot
  casts a vote, can place multiple votes up until nightfall
  payload (target player)
  returns:
  {'status': 'success'}
  failure conditions:
      bad auth
      voting during night
      not in game

* GET /game/{gameid}/ballot
  gets the current state of the ballet
  returns:
  {'status': 'success',
      results: [
      {'playerid': 30, 'votes': 2}
      {'playerid': 31, 'votes': 1}
      ]
  failure conditions:
      bad auth
      not in game

* POST /game/{gameid}/attack
  attacks a villager
  returns:
  {'status': 'success',
      results:
      { 'summary': 'death',
        'combatant': 20 }
    }
  failure conditions:
      bad auth
      not in game
      player is werewolf under cooldown period of 30 minutes
      player is a villager

* POST /game/{gameid}/time
  allows the test script to change the time artificially
  payload (current_time)
  returns:
  {'status': 'success'}


###Basic Game Logic###

The WhereWolf game has two game states, a day and a night
cycle. However, in terms of event-based handling, there is a daybreak
and there is a nightfall. Create two functions that can be called. The
following things happen in each game transition:

**Start game**

Make random werewolf assignment, by calculating the number of
werewolves to create (x), get all of the player ids and store them
randomly in a list. Hint- you can use random.shuffle for this
task. Then set as a werewolf if the player in the list has index 0 to
x

**Nightfall**

Tally up the latest votes in the game, and evict the player with the
majority vote. If the convicted person was a villager and not a
werewolf, for each player who voted against him, set in the player
stat a field lupus and set to one. We will use this value to award the
achievement when the game ends. Check if number of werewolves is zero,
if so, call end game.

**Daybreak**

Do nothing. This is where we might in the future add push
notifications to notify players who died the previous night.

**End game**

Set game status from 1 to 2, showing that the game is no longer
active.  For each eligible player, award achievements. Set all the
game's users' current player field to NULL, effectively making the
player inactive.

### Attacks ###

Every attack will compute the following algorithm that will continue
until someone’s hitpoints drop to 0. In order to understand the attack
process, you should understand the following concepts. There is a
hitpoints. armor rating, and weapon damage.

* Werewolf base stats 

| Rank | HP | Attack |
|------+----+--------|
|    1 | 10 |      3 |
|    2 | 15 |      5 |
|    3 | 20 |      8 |

* Villager base stats

| HP | Attack |
|----+--------|
| 10 |      1 |

* Armor

| Armor     | Armor Rating | Examples       |
|-----------+--------------+----------------|
| Unarmored |           +0 | Tunic          |
| Light     |           +1 | Leather        |
| Medium    |           +2 | Chain mail     |
| Heavy     |           +3 | Silver plating |

* Weapons

| Weapon  | Damage | Examples      |
|---------+--------+---------------|
| Unarmed |     +0 | Fists         |
| Light   |     +3 | Silver dagger |
| Medium  |     +6 | Cleaver       |
| Heavy   |    +10 | Blunderbuss   |

* Special items

| Item                | Effect                                    |
|---------------------+-------------------------------------------|
| Invisibility potion | Makes a villager invisible for 10 minutes |

Consider the following example. You have a level 2 werewolf (15 hp, 5
attack), and a villager with a silver dagger and light armor. Round 1
begins with each at full health. Werewolf has the advantage of
initiative, so attacks first with an attack roll of 5, he rolls a 4,
therefore 4 points are subtracted from the villager's 10 (+1) hit
points leaving him with 7 hit points left. Villager's turn to inflict
damage. The villager has a base of 1 damage plus 1 for the knife, so
an attack roll of 2 is used. The villager rolls a 1, so one point is
subtracted from the werewolf's 15 hp, leaving him with 14 points
left. The werewolf, attacks for 5 damage and the villager is left with
2 points. The villager attacks for 2 damage, leaving the werewolf with
12 points left. The werewolf attacks for 4 damage, leaving the
villager dead with -2 hp.

You can use Python's random module for picking random numbers for each
of the rolls. Modify the items schema to include damage modifiers and
armor rating.

###Achievements###

Your web app should be able to reward users who play the game to
receive the following achievements:

- "Leader of the Pack" have the most number of kills by the end of the game
- "Hair of the dog" - survive an attack by a werewolf
- "A hairy situation" - be near 3 werewolves at once
- "It is never Lupus" - vote for someone to be a werewolf, when they were a townsfolk

You might have to use the player stats or user stats to keep track of
the conditions are met for each of these achievements to be
awarded. For example, in the player stat table, update a field for each
werewolf that stores the total number of kills the player has made
during the game.


###Deliverables###

-server/README
contains mentions of any changes to the gameplay logic that you have made

- server/game.py
contains your web service controller.

- server/schema.sql
contains your changes to the SQL schema.

- server/wherewolfdao.py
contains your changes to DAO for any additional methods your might need.

- test/simplegametest.py
a script based on client.py that uses the Requests library for us to
be convinced your game runs correctly.

You can include any additional files should you want to put your
achievement or attack logic into their own classes and files.

###Grading Rubric###

In order for us to evaluate your work correctly, you must create a
**simplegametest.py** file that calls the methods in the API in order to
play out at least one game. Make sure the script will prove to us that
achievements, attacks, and other aspects of the game work correctly.

+ 20 pts, Game lobby services

Have the functions working that are necessary to create, join, start,
and leave games.

+ 20 pts, Game update logic

Players can update position, and get nearby villagers to attack.

+ 20 pts, Attack algorithm

Werewolves can initiate attacks with villagers, the program considers
the use of items to boost player stats.

+ 20 pts, Voting

Players can vote, and get current ballot status. On nightfall,
majority convicted and expunged. 

+ 10 pts, Awarding achievements

Program can use the temporary stats to award at least the 4
achievements I listed upon game completion.

+ 10 pts, Points of interest

Game can spawn treasures and safe zones. Attacks cannot
work if players are in safe zones. Treasures can be aquired when
player is in the radius of a treasure. 


### Useful Links ###

- [Flask Tutorial][http://flask.pocoo.org/docs/0.10/quickstart/]

- [psycopg tutorial][http://initd.org/psycopg/docs/usage.html]

- [RESTful Web API with Flask][http://blog.luisrei.com/articles/flaskrest.html]


### FAQ ###

- Can I change the schema or the DAO?

YES! Just as long as the web service API works as specified. You might
even have to change it.

- Can I add or change some of the rules?

YES! But you must include a description of your changes and the
reasons behind those changes in your README.

- How should I resolve the current game state (night or day)?

I recommend that you create a helper function called isNight that
is called by the voting and the attacking methods.

What isNight will do is to dynamically evaluate whether the game is
currently in a night or day state. The advantage of doing this
approach is that you do not have to set those things explicitly in a
column in your database, and also helps with the flexibility we will
need when using custom times that are set versus going to wallclock
time. isNight will first check the column in the database for current
time, if the game's current time is set to NULL, then call
datetime.datetime() to fetch the current time, otherwise instantiate a
new datetime object using the value stored in the database for the evaluation.

Set a constant in the beginning of your game.py file called DAYBREAKHOUR and
NIGHTFALLHOUR to 7 and 20 respectively. From the time you are evaluating, either
from the database or from wallclock time, fetch the hour portion and compare whether
you are past NIGHTFALLHOUR or before DAYBREAKHOUR or the converse. Return 1 if
during the night, else 0.












